from __future__ import print_function
from inputs import devices

import serial
ser = serial.Serial('/dev/ttyUSB0')  # open serial port
print(ser.name)         # check which port was really used
from inputs import get_gamepad


def main():
    translation = [0,0,0,0,0]
    while 1:
        events = get_gamepad()
        for event in events:
            if event.code=="ABS_RX":
                if abs(event.state)<5000:
                    translation[0]=0
                else:
                    translation[0]=round(event.state/257.3)
                print(translation)
            if event.code=="ABS_RY":
                if abs(event.state)<5000:
                    translation[1]=0
                else:
                    translation[1]=round(event.state/257.3)
                print(translation)
            if event.code=="ABS_Y":
                if abs(event.state)<5000:
                    translation[2]=0
                else:
                    translation[2]=round(event.state/257.3)
                print(translation)
            if event.code=="ABS_X":
                if abs(event.state)<5000:
                    translation[3]=0
                else:
                    translation[3]=round(event.state/257.3)
                print(translation)
            if event.code=="ABS_Z":
                if abs(event.state)<10:
                    translation[4]=0
                else:
                    translation[4]=round(-event.state/2.005)
                print (translation)
            if event.code=="ABS_RZ":
                if abs(event.state)<10:
                    translation[4]=0
                else:
                    translation[4]=round(event.state/2.005)
                print(translation)
            ser.write(bytes(translation))     # write a string
    ser.close() #close serial port



if __name__ == "__main__":
    
    main()
