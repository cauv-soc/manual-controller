# Manual Controller

Software for manually controlling the sub. Designed around having an Xbox
controller connected to a Raspberry Pi, that parses the input and sends it over
a serial connection to the sub (either serial over USB, or serial over the UART).

More details on the project wiki.